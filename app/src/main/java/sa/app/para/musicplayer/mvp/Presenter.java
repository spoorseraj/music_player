package sa.app.para.musicplayer.mvp;

import java.util.ArrayList;

import javax.inject.Inject;

import sa.app.para.musicplayer.models.MusicEventModel;
import sa.app.para.musicplayer.models.Songs;
import sa.app.para.musicplayer.utils.BaseApplication;

public class Presenter implements Contract.Presenter {
    @Inject
    Contract.Model model;

    private Contract.View view;

    @Override
    public void attachView(Contract.View view) {
        this.view = view;
        BaseApplication.getMyComponent().inject(this);
        model.attachPresenter(this);
    }

    @Override
    public void onLoadData() {
        model.loadData();
    }

    @Override
    public void onSuccess(ArrayList<Songs> arrayList) {
        view.setData(arrayList);
    }

    @Override
    public void onPause() {
        view.pause();
    }

    @Override
    public void onRePlay() {
        view.rePlay();
    }
}
