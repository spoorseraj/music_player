package sa.app.para.musicplayer.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.app.para.musicplayer.R;
import sa.app.para.musicplayer.models.Songs;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.Holder> {
    private static final String TAG = "music_";
    Context context;
    ArrayList<Songs> musics;
    private HashMap<Integer, Boolean> imageStates = new HashMap<>();
    private HashMap<Integer, Boolean> checkBoxStates = new HashMap<>();


    public MusicAdapter(Context context, ArrayList<Songs> musics) {
        this.context = context;
        this.musics = musics;
    }

    @NonNull
    @Override
    public MusicAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_item, viewGroup, false);
        return new Holder(view);
    }

    public int getRow_index() {
        return row_index;
    }

    int row_index = -1;

    @Override
    public void onBindViewHolder(@NonNull MusicAdapter.Holder holder, int i) {
        holder.title.setText(musics.get(i).getSongTitle());
        holder.artist.setText(musics.get(i).getmSongArtist());
//        if (musics.get(i).getBitmap() != null) {
            Log.d(TAG, "onBindViewHolder: " + i);
//            Log.d(TAG, "current: " + row_index);

            holder.img.setImageBitmap(musics.get(i).getBitmap());
//        }
//        else
//            holder.img.setBackgroundResource(R.drawable.musuc);

//        holder.lottie.setVisibility(imageStates.get(i) != null && imageStates.get(i) ? View.VISIBLE : View.GONE);
//        holder.checkBox.setVisibility(checkBoxStates.get(i) != null &&!checkBoxStates.get(i)? View.GONE : View.VISIBLE);
//        if(i==selectionPosition){
//            this.notifyDataSetChanged();
//            selected = musics.get(selectionPosition);
//            selectMusic.getSelectedItem(selected);
//        }
        holder.myRel.setOnClickListener(v -> {
            row_index = i;
            selectionPosition=i;
            this.notifyDataSetChanged();
            selected = musics.get(row_index);
            selectMusic.getSelectedItem(selected);
        });
        if (selectionPosition == i) {
            holder.lottie.setVisibility(View.VISIBLE);

        } else {
            holder.lottie.setVisibility(View.INVISIBLE);

        }

    }

    Songs selected;

    @Override
    public int getItemCount() {
        return musics.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,View.OnClickListener {
        TextView title, artist;
        ImageView img;
        Button buttonOptions;
        LottieAnimationView lottie;
        RelativeLayout myRel;

        public Holder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            artist = itemView.findViewById(R.id.artist);
            img = itemView.findViewById(R.id.img);
            img.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            lottie = itemView.findViewById(R.id.lottie);
            myRel = itemView.findViewById(R.id.myRel);


            buttonOptions = itemView.findViewById(R.id.buttonOptions);
            buttonOptions.setOnClickListener(v -> {
                selected = musics.get(getAdapterPosition());

//                PopupMenu popup = new PopupMenu(context, buttonOptions);
//                popup.getMenuInflater()
//                        .inflate(R.menu.custom_menu, popup.getMenu());
//
//                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        return true;
//                    }
//                });
//                popup.show();

                PopupWindow popupwindow_obj; // create object

                popupwindow_obj = popupDisplay();  // initialize in onCreate()

                popupwindow_obj.showAsDropDown(buttonOptions, -40, -10); // where u want show on view click event


            });


        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Call");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "SMS");
        }

        @Override
        public void onClick(View v) {

        }
    }

    public SelectMusic selectMusic;

    public void getSelectMusic(SelectMusic selectMusic) {
        this.selectMusic = selectMusic;
    }

    public interface SelectMusic {
        void getSelectedPlay(Songs music);

        void getSelectedDelete(Songs music);

        void getSelectedItem(Songs music);
    }

    public PopupWindow popupDisplay() { // disply designing your popoup window
        final PopupWindow popupWindow = new PopupWindow(context); // inflet your layout or diynamic add view

        View view;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(R.layout.my_layout, null);

        TextView play = view.findViewById(R.id.play);
        TextView delete = view.findViewById(R.id.delete);
        play.setOnClickListener(v -> {
            popupWindow.dismiss();
            selectMusic.getSelectedPlay(selected);
        });
        delete.setOnClickListener(v -> {
            popupWindow.dismiss();
            selectMusic.getSelectedDelete(selected);
        });

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(view);

        return popupWindow;
    }
    private int selectionPosition=-1;

    public int getSelectionPosition() {
        return selectionPosition;
    }

    public void setSelectionPosition(int selectionPosition) {
        this.selectionPosition = selectionPosition;
    }

    public interface RecyclerViewClickListener {
        public void recyclerViewListClicked(View v, int position);
    }

}
