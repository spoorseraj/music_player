package sa.app.para.musicplayer.mvp;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.SyncStateContract;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;

import sa.app.para.musicplayer.R;
import sa.app.para.musicplayer.models.MusicEventModel;
import sa.app.para.musicplayer.models.Songs;
import sa.app.para.musicplayer.utils.BaseApplication;
import sa.app.para.musicplayer.utils.Constants;
import sa.app.para.musicplayer.viewPageer.FragmentA;
import sa.app.para.musicplayer.viewPageer.MyPagerAdapter;

public class MainActivity extends AppCompatActivity implements
        Contract.View, Serializable {
    private static final String TAG = "music_";
    private ArrayList<Songs> arrayList;
    private Toolbar toolbar;
    ViewPager mainViewPager;
    SmartTabLayout tabs;
    ImageView play, pause;

    @Inject
    Context mContext;
    @Inject
    Contract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        startService(new Intent(this,MusicPlayerService.class));
//        EventBus.getDefault().post("hihi");

        BaseApplication.getMyComponent().inject(this);
        presenter.attachView(this);
        presenter.onLoadData();

        mainViewPager = findViewById(R.id.mainViewPager);
        play = findViewById(R.id.play);
        pause = findViewById(R.id.pause);
        pause.setOnClickListener(v -> {
            presenter.onPause();
        });
        play.setOnClickListener(v -> {
            presenter.onRePlay();
        });

        tabs = findViewById(R.id.tabs);

        toolbar = findViewById(R.id.toolbar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(toolbar);

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mainViewPager.setAdapter(myPagerAdapter);
        tabs.setViewPager(mainViewPager);
//        EventBus.getDefault().register(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigationdrawer_main, menu);
        return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigationdrawer_main, menu);
        return true;
    }

    @Override
    public void setData(ArrayList<Songs> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("valuesArray", arrayList);
//
        FragmentA fragment = FragmentA.getInstance();
        fragment.setArguments(bundle);


    }


    @Override
    public void pause() {
        Intent intent = new Intent(Constants.Music_CAST);
        sendBroadcast(intent);
        pause.setVisibility(View.INVISIBLE);
        play.setVisibility(View.VISIBLE);
    }

    @Override
    public void rePlay() {
        Intent intent = new Intent(Constants.Music_CAST);
        sendBroadcast(intent);
        pause.setVisibility(View.VISIBLE);
        play.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume: ");
    }


}
