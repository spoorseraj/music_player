package sa.app.para.musicplayer.dagger;

import dagger.Component;
import sa.app.para.musicplayer.mvp.MainActivity;
import sa.app.para.musicplayer.mvp.Model;
import sa.app.para.musicplayer.mvp.Presenter;

@Component(modules = {MyModule.class})
public interface MyComponent {
    void inject(MainActivity mainActivity);
    void inject(Presenter presenter);
    void inject(Model model);


}