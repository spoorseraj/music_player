package sa.app.para.musicplayer.viewPageer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.model.KeyPath;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.ArrayList;

import sa.app.para.musicplayer.mvp.Contract;
import sa.app.para.musicplayer.mvp.MusicAdapter;
import sa.app.para.musicplayer.R;
import sa.app.para.musicplayer.models.Songs;
import sa.app.para.musicplayer.mvp.MusicPlayerService;
import sa.app.para.musicplayer.utils.Constants;

public class FragmentA extends Fragment implements Serializable, MusicAdapter.SelectMusic {
    private static final String TAG = "Flag_";
    private Context mContext;
    MusicAdapter musicAdapter;
    private ArrayList<Songs> arrayList, musics;
    int currentIndex;


    public static FragmentA fragmantA;

    public static FragmentA getInstance() {
        if (fragmantA == null) {
            fragmantA = new FragmentA();
        }
        return fragmantA;
    }

    private RecyclerView recycler;
    Boolean flag = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_a, container, false);
        recycler = view.findViewById(R.id.recycler);
        mContext = getActivity();
        arrayList = new ArrayList<>();
        musics = new ArrayList<>();

//        LottieAnimationView animationView = (LottieAnimationView) view.findViewById(R.id.lottie);
//        ColorFilter colorFilter = new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.LIGHTEN);
//        animationView.addColorFilter(colorFilter);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            arrayList = (ArrayList<Songs>) bundle.getSerializable("valuesArray");
            musics = arrayList;
        }
        musicAdapter = new MusicAdapter(getActivity(), arrayList);
        recycler.setAdapter(musicAdapter);
        musicAdapter.getSelectMusic(this);
//        Log.d(TAG, "onCreateView: A");
//        EventBus.getDefault().register(this);
        return view;
    }


    @Override
    public void getSelectedPlay(Songs music) {
        play(music);

    }

    @Override
    public void getSelectedDelete(Songs music) {
        Toast.makeText(getActivity(), "delete", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSelectedItem(Songs music) {
        Log.d(TAG, "getSelectedItem: ");
        play(music);

    }

    private void play(Songs music) {
        currentIndex = musicAdapter.getSelectionPosition();
        Intent intent = new Intent(mContext, MusicPlayerService.class);
        intent.putExtra(Constants.MUSIC_KEY, music.getmSongUrl());
        intent.putExtra(Constants.MUSIC_INDEX, currentIndex);
        mContext.startService(intent);
//        if(!flag) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    EventBus.getDefault().post(arrayList);
//                }
//            }, 3000);
//            flag=true;
//        }

        Toast.makeText(getActivity(), musicAdapter.getSelectionPosition() + "", Toast.LENGTH_SHORT).show();
    }

    private void changeSelectedSong(int index) {
//        musicAdapter.notifyItemChanged(musicAdapter.getSelectionPosition());
        currentIndex = index;
        musicAdapter.setSelectionPosition(currentIndex);
        musicAdapter.notifyItemChanged(currentIndex);
        musicAdapter.notifyDataSetChanged();

        recycler.scrollToPosition(index);
//        recycler.scrollTo(0,);

        
        play(musics.get(index));

    }

    @Subscribe
    public void onCompleteMusic(String status) {
        Log.d(TAG, "onCompleteMusic: ");
        if (currentIndex + 1 < musics.size()) {
            Songs next = musics.get(currentIndex + 1);
            changeSelectedSong(currentIndex + 1);
//            prepareSong(next);
//            mainActivity.setsongText(next);
        } else {
            Songs next = musics.get(0);
            changeSelectedSong(0);
//                    prepareSong(next);
//                     mainActivity.setsongText(next);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
