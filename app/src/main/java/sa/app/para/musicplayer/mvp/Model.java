package sa.app.para.musicplayer.mvp;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;

import java.util.ArrayList;

import javax.inject.Inject;

import sa.app.para.musicplayer.R;
import sa.app.para.musicplayer.models.Songs;
import sa.app.para.musicplayer.utils.BaseApplication;

public class Model implements Contract.Model {
    private Contract.Presenter presenter;
    private String TAG="music_";

    @Inject
    Context context;

    @Override
    public void attachPresenter(Contract.Presenter presenter) {

        this.presenter = presenter;
        BaseApplication.getMyComponent().inject(this);
    }

    @Override
    public void loadData() {
        getMusic();
    }

    public void getMusic() {
        ArrayList<Songs> arrayList = new ArrayList<>();
        ContentResolver contentResolver = BaseApplication.getContext().getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor songCursor = contentResolver.query(songUri, null, null, null, null);

        if (songCursor != null && songCursor.moveToFirst()) {
            int songId = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int songImage = songCursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA);
            int songUrl = songCursor.getColumnIndex(MediaStore.Audio.Media.DATA);

            do {
                long currentId = songCursor.getLong(songId);
                String currentTitle = songCursor.getString(songTitle);
                String currentArtist = songCursor.getString(songArtist);
                String currentImage = songCursor.getString(songImage);
                String currentUrl = songCursor.getString(songUrl);
                Log.d(TAG, "getMusic: "+currentUrl);
                Uri imageUri = ContentUris
                        .withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                songCursor.getInt(songCursor.getColumnIndex(MediaStore.Images.ImageColumns._ID)));

                MediaMetadataRetriever mmr = new MediaMetadataRetriever();

                byte[] rawArt;
                Bitmap b=convertToBitmap(ResourcesCompat.getDrawable(context.getResources(), R.drawable.musuc, null),
                        50,50);
                Bitmap art = b;
                BitmapFactory.Options bfo = new BitmapFactory.Options();

                mmr.setDataSource(currentImage);
                rawArt = mmr.getEmbeddedPicture();

                if (null != rawArt)
                    art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);

                arrayList.add(new Songs(currentId, currentTitle, currentArtist,currentUrl, art));
            } while (songCursor.moveToNext());
        }
        presenter.onSuccess(arrayList);
    }

    public Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);

        return mutableBitmap;
    }
}
