package sa.app.para.musicplayer.dagger;

import android.content.Context;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import sa.app.para.musicplayer.models.Songs;
import sa.app.para.musicplayer.mvp.Contract;
import sa.app.para.musicplayer.mvp.Model;
import sa.app.para.musicplayer.mvp.Presenter;

@dagger.Module
public class MyModule {
    Context mContext;

    public MyModule(Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    public Context getContext() {
        return mContext;
    }

    @Provides
    Contract.Model getModel() {
        return new Model();
    }

    @Provides
    Contract.Presenter getPresenter() {
        return new Presenter();
    }



}
