package sa.app.para.musicplayer.viewPageer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sa.app.para.musicplayer.R;

public class FragmentB extends Fragment {
    private static final String TAG = "Flag_";
    static FragmentB fragmentB;
//    RecyclerView recycler;
    public static FragmentB getInstance(){
        if (fragmentB == null) {
            fragmentB=new FragmentB();
        }
        return  fragmentB;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_b,container,false);
//        recycler=view.findViewById(R.id.recycler);
        Log.d(TAG, "onCreateView: B");
        return view;
    }
}
