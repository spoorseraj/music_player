package sa.app.para.musicplayer.models;

public class MusicEventModel {
    Boolean play;

    public Boolean getPlay() {
        return play;
    }

    public void setPlay(Boolean play) {
        this.play = play;
    }
}
