package sa.app.para.musicplayer.utils;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import sa.app.para.musicplayer.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }
}
