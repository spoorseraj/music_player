package sa.app.para.musicplayer.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Songs implements Parcelable,Serializable {
    private long mSongID;
    private String mSongTitle,mSongArtist,mSongUrl;
    private Bitmap bitmap;

    public Songs(long mSongID, String mSongTitle, String mSongArtist, String mSongUrl, Bitmap bitmap) {
        this.mSongID = mSongID;
        this.mSongTitle = mSongTitle;
        this.mSongArtist = mSongArtist;
        this.mSongUrl = mSongUrl;
        this.bitmap = bitmap;
    }

    public String getmSongUrl() {
        return mSongUrl;
    }

    protected Songs(Parcel in) {
        mSongID = in.readLong();
        mSongTitle = in.readString();
        mSongArtist = in.readString();
        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<Songs> CREATOR = new Creator<Songs>() {
        @Override
        public Songs createFromParcel(Parcel in) {
            return new Songs(in);
        }

        @Override
        public Songs[] newArray(int size) {
            return new Songs[size];
        }
    };

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getmSongArtist() {
        return mSongArtist;
    }

    public long getSongID() {
        return mSongID;
    }

    public String getSongTitle() {
        return mSongTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mSongID);
        dest.writeString(mSongTitle);
        dest.writeString(mSongArtist);
        dest.writeParcelable(bitmap, flags);
    }
}