package sa.app.para.musicplayer.viewPageer;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyPagerAdapter extends FragmentPagerAdapter {


    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0)
            return FragmentA.getInstance();
        else if (i == 1)
            return FragmentB.getInstance();
        else if (i == 2)
            return FragmentC.getInstance();
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position== 0)
            return "list music";
        else if (position == 1)
            return "album";
        else if (position == 2)
            return "others";
        return "";
    }
}
