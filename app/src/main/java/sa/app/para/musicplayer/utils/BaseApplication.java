package sa.app.para.musicplayer.utils;

import android.app.Application;


import sa.app.para.musicplayer.dagger.DaggerMyComponent;
import sa.app.para.musicplayer.dagger.MyComponent;
import sa.app.para.musicplayer.dagger.MyModule;

public class BaseApplication extends Application {
    static BaseApplication context;

    public static BaseApplication getContext() {
        return context;
    }

    private static MyComponent myComponent;

    public static MyComponent getMyComponent() {
        return myComponent;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        myComponent=DaggerMyComponent.builder().myModule(new MyModule(this)).build();
    }
}
