package sa.app.para.musicplayer.mvp;

import java.util.ArrayList;

import sa.app.para.musicplayer.models.Songs;

public interface Contract {
    interface View {
        void setData(ArrayList<Songs> arrayList);
        void pause();
        void rePlay();
    }

    interface Presenter {
        void attachView(View view);
        void onLoadData();
        void onSuccess(ArrayList<Songs> arrayList);
        void onPause();
        void onRePlay();
    }

    interface Model {
        void attachPresenter(Presenter presenter);
        void loadData();
    }

}
