package sa.app.para.musicplayer.mvp;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;

import sa.app.para.musicplayer.models.Songs;
import sa.app.para.musicplayer.utils.Constants;

public class MusicPlayerService extends Service implements Serializable {
    private static final String TAG = "music_";
    MediaPlayer mediaPlayer;
    ArrayList<Songs> musics;
    int currentIndex;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        broadcastDefinedPause();
//        EventBus.getDefault().register(this);
//        broadcastDefinedPlay();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
//        https://github.com/googlesamples/android-MediaBrowserService
        if (intent.hasExtra(Constants.MUSIC_KEY)) {
            String musicAddress = intent.getStringExtra(Constants.MUSIC_KEY);
            currentIndex=intent.getIntExtra(Constants.MUSIC_INDEX,0);
            play(musicAddress);
        }
        if (intent.hasExtra("test")) {
            ArrayList<Songs> filelist = intent.getParcelableExtra("test");
            Toast.makeText(this, "test", Toast.LENGTH_SHORT).show();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void play(String musicAddress) {
        if (mediaPlayer == null) mediaPlayer = new MediaPlayer();
        else {
            mediaPlayer.stop();
            mediaPlayer.reset();

        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.d(TAG, "onCompletion: ");
                EventBus.getDefault().post("end");
//                EventBus.getDefault().post();
//                if(currentIndex + 1 < musics.size()){
//                    Songs next = musics.get(currentIndex + 1);
//                    changeSelectedSong(currentIndex+1);
//                    prepareSong(next);
//                    mainActivity.setsongText(next);
//                }else{
//                    Songs next = musics.get(0);
//                    changeSelectedSong(0);
//                    prepareSong(next);
//                     mainActivity.setsongText(next);
//                }
            }
        });
//        if (!mediaPlayer.isPlaying())
//            mediaPlayer.start();


        Log.d(TAG, "play: " + musicAddress);
        try {
            mediaPlayer.setVolume(1.0f, 1.0f);
            mediaPlayer.setDataSource(musicAddress);
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (Exception ex) {
            Toast.makeText(this, "fail", Toast.LENGTH_SHORT).show();
            stopSelf();
        }
    }

    BroadcastReceiver musicReceiver;
//    BroadcastReceiver musicPlayReceiver;

    void broadcastDefinedPause() {
        musicReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (mediaPlayer.isPlaying())
                    mediaPlayer.pause();
                else
                    mediaPlayer.start();
            }
        };
        IntentFilter intentFilter = new IntentFilter(Constants.Music_CAST);
        registerReceiver(musicReceiver, intentFilter);
    }

//    void broadcastDefinedPlay() {
//        musicPlayReceiver=new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if(mediaPlayer.)
//                    mediaPlayer.pause();
//            }
//        };
//        IntentFilter intentFilter=new IntentFilter(Constants.Music_Play);
//        registerReceiver(musicPlayReceiver,intentFilter);
//    }

    private void stop() {
        mediaPlayer.stop();
        mediaPlayer.release();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void getMusics(ArrayList<Songs> arrayList) {
        Log.d(TAG, "getMusics: ");
        musics=arrayList;

    }


}
